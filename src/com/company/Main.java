package com.company;

public class Main {

    public static void main(String[] args) {

        Quiz quiz = new QuizImpl();
        int firstValue = Quiz.MIN_VALUE;
        int lastValue = Quiz.MAX_VALUE;
        int digit = (firstValue + lastValue) / 2;


        for (int counter = 1; ; counter++) {

            try {

                quiz.isCorrectValue(digit);
                System.out.println("Trafiona proba!!! Szukana liczba to: "
                        + digit + " Ilosc prob: " + counter);
                break;

            } catch (Quiz.ParamTooLarge e) {

                System.out.println("Argument za duzy!!!");
                lastValue = digit;
                digit = (firstValue + lastValue) / 2;
            } catch (Quiz.ParamTooSmall e) {

                System.out.println("Argument za maly!!!");
                firstValue = digit;
                digit = (firstValue + lastValue) / 2;
            }
        }
    }
}

